from pathlib import Path

import sass
from django.contrib.staticfiles.storage import StaticFilesStorage


class SassStaticFilesStorage(StaticFilesStorage):
    """
    A static file system storage backend which also
    compile scss files
    """

    def post_process(self, found_files, dry_run=False, **options):
        # don't even dare to process the files if we're in dry run mode
        if dry_run:
            return
        for storage, f in found_files.values():
            f = Path(f)
            if f.name.endswith('.scss') and not f.name.startswith('_') and not f.parent.name.startswith('_'):
                processed = compile_scss(filename=storage.path(f), dest_dir=Path(self.path(f)).parent)
                yield f, processed, True
            else:
                yield f, None, False


def compile_scss(*, filename: str, dest_dir=None):
    pf = Path(filename)
    if dest_dir is None:
        dest_dir = pf.parent
    r = sass.compile(filename=filename, output_style='compressed')
    with open(output_filename := dest_dir / f'_{pf.name[:-5]}.min.css', 'w') as output_file:
        output_file.write(r)
    return output_filename
