#!/bin/bash

cd /code

source resources/tool_shed.sh

if [ "$1" == "test" ]; then
    msg_info "Dependencies"
    wkhtmltopdf --version
    pip freeze
    msg_info "Running tests"
    #export PATH="${PATH}:/home/kiwi/.local/bin"
    #pip install --no-cache-dir -r requirements-dev.txt
    source <(cat mysite/settings.example.ini | grep -v "\[settings\]" | sed 's/^/export /g') || exit 2
    export USE_DJANGO_EXTENSIONS=True
    python manage.py collectstatic --noinput --skip-checks --link || exit 9

    python manage.py makemigrations || exit 7
    coverage run --concurrency=multiprocessing manage.py test --parallel --keepdb $2 || exit 3
    coverage combine
    coverage report --skip-covered --omit=*/wsgi.py,*/asgi.py,manage.py,*/apps.py
    coverage xml --omit=*/wsgi.py,*/asgi.py,manage.py,*/apps.py
    coverage html --omit=*/wsgi.py,*/asgi.py,manage.py,*/apps.py
    exit 0
fi

if [ "$1" == "hold_on" ]; then
    msg_info "holding on util you delete /tmp/hold_on"
    touch /tmp/hold_on
    while [ -e "/tmp/hold_on" ]; do
        sleep 1 ;
        echo "holding on" ;
    done
fi

if [ ! -e mysite/settings.ini ] ; then
    if [ "$ALLOWED_HOSTS" == ""  ] ; then
        msg_error "Missing settings.ini and \$ALLOWED_HOSTS"
        exit 1
    fi
    if [ "$SECRET_KEY" == ""  ] ; then
        msg_error "Missing settings.ini and \$SECRET_KEY"
        exit 1
    fi
fi

msg_info "Copy static files to shared directory"
export STATIC_ROOT_SHARED=$(SECRET_KEY=bla ALLOWED_HOSTS=* USE_SQLITE_AS_DB=True python manage.py shell -c "from django.conf import settings; print(settings.STATIC_ROOT_SHARED)")
export STATIC_ROOT=$(SECRET_KEY=bla ALLOWED_HOSTS=* USE_SQLITE_AS_DB=True python manage.py shell -c "from django.conf import settings; print(settings.STATIC_ROOT)")
echo "cp -rf $STATIC_ROOT/* $STATIC_ROOT_SHARED"

cp -rf $STATIC_ROOT/* $STATIC_ROOT_SHARED || msg_warning "Copy to shared static dir failed"

msg_info "Applying database migrations"
python manage.py makemigrations || exit 8 # dev purpose only !!!!
python manage.py migrate || exit 4

if [ "$1" == "do_not_start" ]; then
    msg_info "Entrypoint have been run successfully, we are not starting the project as requested"
elif [ "$1" == "django" ]; then
    exec python manage.py runserver 0.0.0.0:8086
else
    if [ "$START_HUEY" == "False" ]; then
        msg_info "Do not start huey"
        exec "$@"
    else
        msg_info "Starting web server"
        (exec "$@")&
        msg_info "Starting web server done"
        msg_info "Running huey"
        python manage.py run_huey
    fi
fi

# Start server
#echo "Starting server"
#python manage.py runserver 0.0.0.0:8001
