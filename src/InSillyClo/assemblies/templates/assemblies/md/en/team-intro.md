Our main long-term goal is to develop a comprehensive methodological framework supporting the development of a quantitative understanding of cellular processes.

Our research rests on three pillars:

 * Lab  automation - engineer experimental platforms and develop computational pipeline for strain construction and data analysis,
 * Bioengineering - use synthetic  biology and standardized cloning approaches to probe the functioning of natural systems and construct novel systems,
 * Quantitative modeling and real-time control – develop deterministic and stochastic models of cell processes to explain observations and guide designs.