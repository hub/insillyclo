##### Creating Models: From Idea to Reality

Finally, all this research and testing converged on the creation of the mock-ups. These mock-ups, faithful to the validated concepts, made it possible to visualize the tool as it would be used on a daily basis. They were designed to be both functional and aesthetic, aligning ergonomics with InSillyClo's visual identity.