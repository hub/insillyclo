function renderDot(target, source, zoomEnabled){
    let graphviz = d3.select(target).graphviz().attributer(attributer)
    graphviz = graphviz.zoom(zoomEnabled)

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            graphviz.renderDot(xhr.responseText);
        }
    }
    xhr.open('GET', source);
    xhr.send();
}

function attributer(datum, index, nodes) {
    margin = 20; // to avoid scrollbars
    var selection = d3.select(this);
    if (datum.tag == "svg") {
        var width = this.parentNode.innerWidth;
        var height = Math.max(Math.min(this.parentNode.innerHeight||200, window.innerHeight*0.8), window.innerHeight*0.3);
        height = window.innerHeight * 0.8;
        console.log(width);
        console.log(height);
        selection
            .attr("width", width)
            .attr("height", height)
        datum.attributes.width = width - margin;
        datum.attributes.height = height - margin;
    }
}