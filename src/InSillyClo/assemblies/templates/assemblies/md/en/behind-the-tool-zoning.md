##### Zoning: Laying the visual foundations

Even before adding colors or details, we worked on zoning. This stage involved defining the location and hierarchy of elements on each screen. We tested several layouts to ensure that essential information was easily accessible and that the interface remained balanced, without cognitive overload.