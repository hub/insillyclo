from basetheme_bootstrap.admin import ViewOnSiteModelAdmin
from django.contrib import admin
from django.contrib.auth import admin as auth_admin

from assemblies import models


# Register your models here.
@admin.register(models.Assembly)
class AssemblyAdmin(ViewOnSiteModelAdmin, admin.ModelAdmin):
    date_hierarchy = 'updated_at'
    search_fields = (
        'name',
        'input_parts__assemblies__name',
    )
    list_display = (
        'name',
        'is_public',
        'is_input_part_typed',
        'output_separator',
        'restriction_enzyme',
    )


@admin.register(models.RestrictionEnzyme)
class RestrictionEnzymeAdmin(ViewOnSiteModelAdmin, admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(models.InputPart)
class InputPartAdmin(ViewOnSiteModelAdmin, admin.ModelAdmin):
    date_hierarchy = 'updated_at'
    search_fields = ('name',)
    list_display = (
        'name',
        'number_of_subparts_str',
        'is_mandatory',
        'is_in_output_plasmid_name',
    )


@admin.register(models.User)
class UserAdmin(auth_admin.UserAdmin):
    ordering = ("email",)
    list_display = (
        'email',
        'first_name',
        'last_name',
        'is_active',
        'is_staff',
        'is_superuser',
    )
