We built around it an environment allowing the user to save time behind the computer and in the lab, to focus on what
matters : the designs.
<br/>
This environment is divided in the optional upstream module <b class="text-designer">Assembly Designer</b> to define assembly type and the core downstream <b class="text-simulator">Assembly Simulator</b> to perform insilico cloning and related computations.
<br/>
InSillyClo was made as light as possible to support both small and large scale cloning while allowing users to keep their habits.

<img class="w-100" src="/static/img/tool-details.svg">

__Assembly Simulator takes as entries:__

The oP file containing for each plasmid to be made (output plasmid) the names of the input plasmids to be assembled
together,

* Genbank files of input plasmids,
* The iP file associating each input plasmid name with the name of the corresponding genbank file.
* The user defines output plasmids to be constructed using names of input plasmids in oP file. Thanks to the iP file and
genbanks files, the script tracks back sequences to be assembled together to generate the sequence of the output
plasmid.

__Additionnal features include:__

* Simulation of agarose gel after PCR or restriction digest,
* Calculation of input plasmid dilutions if the user wants to perform equimolar mix.

By using predifined assembly type, Assembly Simulator can be ran independently of Assembly Designer. We advice new users to do so for their first InSillyClo workflow. However, defining assembly type will allow users to have tailored and more efficient cloning workflows.

__Assembly Designer has three mains goals:__

* Specifying name of each input part, if it is mandatory and if it can be divided into two parts since most modular cloning kits uses subparts to allow combinatorial designs,
* A better proof-reading of the assembly, for instance in case of a missing part, and a clearer oP file.
* Convention for outplut plasmid naming.
