function pushJob(uuid){
    uuids = getJobs()
    uuids.add(uuid)
    localStorage.setItem('job_uuids', Array.from(uuids).toString());
}

function getJobs(){
    var uuids = localStorage.getItem('job_uuids');
    if (uuids)
        return new Set(uuids.split(','))
    return new Set()
}

function delJob(uuid){
    uuids = getJobs()
    if (uuids.delete(uuid))
        localStorage.setItem('job_uuids', Array.from(uuids).toString());
}


function fillJobList(parentId, url_to_poll){
    let parent = document.getElementById(parentId);
    getJobs().forEach((uuid) => {
            appendJob(uuid, parent, url_to_poll)
    })
}

function appendJob(uuid, parent, url_to_poll){
    let div = document.createElement('div');
    div.setAttribute('hx-get', url_to_poll.replace("xxxxxx", uuid));
    div.setAttribute('hx-trigger', 'load delay:20ms, every 1s');
    div.setAttribute('hx-target', '#job-'+uuid);
    div.setAttribute('hx-swap', 'outerHTML');
    div.setAttribute('hx-on:htmx:response-error', "delJob('"+uuid+"');htmx.remove('#job-"+uuid+"')");
    parent.append(div);

    let elt = document.createElement('dd');
    elt.setAttribute('id', "job-"+uuid);
    elt.className = 'd-flex border-bottom';
    elt.innerHTML='<i class="p-1 bi bi-question-circle text-muted fs-5"></i><span class="p-1 fs-5 text-primary">'+uuid.substring(0,8)+'</span>';
    parent.append(elt);

    htmx.process(div);
}

function appendJobFromSearchBar(parentId, url_to_poll){
    let input = document.getElementById('input-uuid')
    if (!document.getElementById('form-uuid').checkValidity()){
        input.classList.add("is-invalid");
        return false
    }
    input.classList.remove("is-invalid");

    let uuid = input.value
    input.value = ''
    if (getJobs().has(uuid))
        return false
    pushJob(uuid)
    return appendJob(uuid, document.getElementById(parentId), url_to_poll)
}
