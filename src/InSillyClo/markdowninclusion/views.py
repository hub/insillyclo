import django.conf
from django.template.loader import get_template
from django.views import generic


class MdTemplateView(django.views.generic.TemplateView):
    template_name = 'markdowninclusion/md_host.html'
    md_name = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_context_data(self, **kwargs):
        d = super().get_context_data()
        d.update(
            title=get_template(self.md_name[:-3] + ".title.txt").template.source,
            md_name=self.md_name,
        )
        return d
