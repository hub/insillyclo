##### Working on Navigation: A clear, logical structure

One of the keys to a good tool is intuitive navigation. We therefore redesigned the overall structure of the application to reflect users' actual processes. Clear workflows, simplified menus and progressive information organization were implemented to reduce the learning curve and maximize efficiency.