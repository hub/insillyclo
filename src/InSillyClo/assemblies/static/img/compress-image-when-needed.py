#!/usr/bin/env python3
from pathlib import Path
import os

r = os.system('convert --version > /dev/null')
if r != 0:
    print("convert have to be installed")
    exit(1)
file_count = 0
compressed_count = 0
kept_as_is_count = 0
for p in Path(".").glob("**/*"):
    if not p.is_file():
        continue
    if p.suffix.lower() not in ['.png', '.jpg', '.jpeg']:
        continue
    path = str(p.resolve())
    path_without_extension = path[: path.rfind(".")]
    compressed_image = Path(path_without_extension + '.jpg')
    flag = Path(path_without_extension + '.compressed')
    file_count += 1
    if flag.exists():
        continue
    compressed_image_candidate = Path(f'{compressed_image.resolve()}.candidate.jpg')
    os.system(f'convert -resize 1920x1080\\> -quality 95% {p.resolve()} {compressed_image_candidate.resolve()}')
    dest_size = compressed_image_candidate.stat().st_size
    src_size = p.stat().st_size
    if dest_size < src_size * 0.9:
        if p.resolve() != compressed_image.resolve():
            os.system(f'git mv {p.resolve()} {compressed_image.resolve()}')
        compressed_image_candidate.replace(compressed_image.resolve())
        print(f"{p} have been compressed from {src_size} to {dest_size}")
        compressed_count += 1
    else:
        compressed_image_candidate.unlink()
        kept_as_is_count += 1
    flag.touch()
    os.system(f'git add {flag.resolve()}')

print(
    f'{file_count} images found, {compressed_count} were compressed, '
    f'and {kept_as_is_count} where kept as is. The others are flagged as already studied'
)
