function stringToHTML (str) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(str, 'text/html');
    return doc.body;
}

function add_form_to_nested_formset(source, prefix) {
    let $formset_container = source.closest(".formset-container");
    let $empty_form = $formset_container.querySelector(".empty_form");
    if (typeof prefix == "undefined") {
        prefix = $empty_form.dataset.prefix;
    }
    let input_total = $formset_container.querySelector("[name='" + prefix + "-TOTAL_FORMS']"),
        input_total_value = parseInt(input_total.value);
    let input_max = $formset_container.querySelector("[name='" + prefix + "-MAX_NUM_FORMS']");
    if (input_max.value === input_total_value) {
        return null;
    } else if (input_max.value === input_total_value + 1) {
        source.closest(".formset-container").querySelector(".formset-new-item").hide();
    }
    let empty_form_as_str = $empty_form.innerHTML;
    empty_form_as_str = empty_form_as_str.replace(
        new RegExp(prefix + "-__prefix__", "g"),
        prefix + "-" + input_total_value
    );
    let empty_form = stringToHTML(empty_form_as_str);
    let formset_new_item = source.closest(".formset-container").querySelector(".formset-new-item");
    formset_new_item.parentNode.insertBefore(empty_form, formset_new_item);
    input_total.value = input_total_value + 1;
    return empty_form;
}

document.addEventListener("DOMContentLoaded", function () {
    document.querySelector('.formset-new-item')
        .addEventListener('click', function (e) {
            add_form_to_nested_formset(e.target);
        });
});