from django.urls import path
from django.views.generic import TemplateView

from . import views, wizard_views

designer_wizard = wizard_views.AssemblyDesignerNotProtected.as_view(
    url_name='assemblies:designer-create-step',
)


app_name = 'assemblies'
urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about, name='about'),
    path('form-example', views.form_example, name='form_example'),
    path(
        'showcase',
        TemplateView.as_view(
            template_name="assemblies/showcase.html",
            extra_context=dict(
                theme_colors=[
                    "",
                    "simulator",
                    "designer",
                    "primary",
                    "secondary",
                    "success",
                    "info",
                    "warning",
                    "danger",
                    "light",
                    "dark",
                ],
                range6=list(range(1, 7)),
            ),
        ),
        name='showcase',
    ),
    path(
        'smp',
        TemplateView.as_view(
            template_name="assemblies/simple_message_page.html",
            extra_context=dict(
                page_title='page_title',
                message='message',
                sub_message='sub_message',
            ),
        ),
        name='simple-message-page',
    ),
    path('behind-the-tool', TemplateView.as_view(template_name="assemblies/behind-the-tool.html"), name='behind-tool'),
    path('team', TemplateView.as_view(template_name="assemblies/team.html"), name='team'),
    path('policy', TemplateView.as_view(template_name="assemblies/policy.html"), name='policy'),
    path('tutorial', TemplateView.as_view(template_name="assemblies/tutorial.html"), name='tutorial'),
    path('assembly-designer/create/<str:step>/', designer_wizard, name='designer-create-step'),
    path('assembly-designer/create/', designer_wizard, name='designer-create'),
    path('assembly/', views.AssemblyListView.as_view(), name='assembly-list'),
    path('assembly/<int:pk>/', views.AssemblyDetailView.as_view(), name='assembly-detail'),
    path('fragment', views.show_fragment, name='fragment'),
]
