import logging

import markdown as md
from django import template
from django.conf import settings
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.utils.safestring import mark_safe
from django.utils.translation import get_language

register = template.Library()

logger = logging.getLogger(__name__)


@register.simple_tag(takes_context=True)
def include_md(context, filepath):
    if "%lg%" in filepath:
        try:
            return include_md(context, filepath.replace("%lg%", get_language()))
        except TemplateDoesNotExist as e:
            return include_md(context, filepath.replace("%lg%", settings.LANGUAGE_CODE))
    return mark_safe(
        md.markdown(
            get_template(filepath).template.source,
            extensions=[
                'markdown.extensions.fenced_code',
                'markdown.extensions.sane_lists',
                'pymdownx.arithmatex',
            ],
            extension_configs={'pymdownx.arithmatex': {'generic': True}},
        )
    )
