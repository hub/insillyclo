## ASSEMBLY DESIGNER

Here is a Description of the entries of the app:

* Name : name of the assembly
* Backbone : if you want to consider one of your input plasmids as backbone (input plasmid containing the antibiotic marker that will be in the output plasmid)
* Backbone in output: if your want the name of the backbone to be in the name of the ouptut plasmid
* Backbone position : if you chose to add the backbone in the name of output plasmid, then you say if you want it at the beginning or at the end
* Output separator : when computing the name of the outpus plasmid, input part names will be separated by what you enter Here
* Interpretable : if you want your output plasmid to have an interpretation
* Part Side : work in progress

### INPUT PART
To add a input part, click on Add Input Part to open a new window:

* Name : name of the input part
* Mandatory : check to make the part mandatory (if mandatory, Assembly Simulator will raise an error if this part is missing in your assembly)
* Separable : if the part can be separated in subparts
* Separator : character that will determine subparts composing a part
* Interpretable : if you are using interpretation to define standardized parts
* Number of interpretations : enter the maximum number of subparts (because it corresponds to the maxium number of interpretations)
* Click on Add interpretation to open the window and specify them. The logic is that on the line 1 Interpretation 1 their is no separator, on line 2 (in case you chose 2 interpretations or higher) with Interpretation 2 you have 2 entries. This is for when a part is composed of two subparts divied by the separator you specified. In the first entry enter the interpretation of the first subpart and in the second the interpretation of the second subpart. If you have a line 3 it means that you can have 3 subparts composing one part and so on...

### OUTPUT
Assembly Designer will generate:

* .pkl file : which store the features of your assembly
* .txt file : wich gives you in human readable language the features of your assembly
* DB_oP_template.csv : a template file to be used to define the plasmid you want to build. It will be used as input for Assembly Simulator.