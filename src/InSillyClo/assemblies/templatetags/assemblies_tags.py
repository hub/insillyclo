import logging

from django import template
from django.db.models import QuerySet
from django.utils.safestring import mark_safe

register = template.Library()

logger = logging.getLogger(__name__)


@register.filter
def class_verbose_name(obj):
    if hasattr(obj, 'model'):
        obj = obj.model
    return obj._meta.verbose_name.title()


@register.filter
def class_verbose_name_plural(obj):
    if hasattr(obj, 'model'):
        obj = obj.model
    return obj._meta.verbose_name_plural.title()


def __get_field_from_instance(field_name, obj):
    if type(obj) == QuerySet:
        f = obj.model._meta.get_field(field_name)
    else:
        f = obj._meta.get_field(field_name)
    return f


@register.filter
def field_verbose_name(obj, field_name):
    f = __get_field_from_instance(field_name, obj)
    return f.verbose_name.title()


@register.filter
def field_help_text(obj, field_name):
    return __get_field_from_instance(field_name, obj).help_text


@register.filter
def field_verbose_name_and_help_text(obj, field_name):
    f = __get_field_from_instance(field_name, obj)
    verbose_name = f.verbose_name.title()
    help_text = f.help_text
    if help_text == "":
        return verbose_name
    return mark_safe(f'{verbose_name} <i class="bi bi-question-circle-fill" title="{help_text}"></i>')
