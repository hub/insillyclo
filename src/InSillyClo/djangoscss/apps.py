from django.apps import AppConfig


class DjangoscssConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangoscss'
