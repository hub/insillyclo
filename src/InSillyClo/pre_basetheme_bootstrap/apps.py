from django.apps import AppConfig


class PreBasethemeBootstrapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pre_basetheme_bootstrap'
