from crispy_forms.helper import FormHelper
from django.db import transaction
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from formtools.wizard import views as wizard_views


from assemblies import forms


class AssemblyDesignerNotProtected(wizard_views.NamedUrlSessionWizardView):
    template_name = 'wizard_view_form_host.html'
    form_list = [
        ("Intro", forms.AgreeForm),
        ("Properties", forms.AssemblyForm),
        ("InputParts", forms.InputPartFormFormSet),
        ("Summary", forms.AgreeForm),
    ]
    is_edition = False
    condition_dict = dict()

    def get_context_data(self, form, step=None, **kwargs):
        context = super().get_context_data(form=form, step=step, **kwargs)
        context["submit_text"] = _("Next")
        context["title"] = _("AssemblyDesignerWizard.title")
        context["custom_css_width"] = "col-12 context-designer"
        context["form_title_class"] = "text-primary"
        context["custom_container_class"] = "context-designer"
        context["hide_first_step_button"] = True
        if step is None:
            step = self.steps.current
        if step == "Intro":
            context["form_title_class"] = "h1"
            context["title"] = ""
            context["form_title"] = _("AssemblyDesignerWizard.Intro.form_title")
            context["step_message"] = mark_safe(_("AssemblyDesignerWizard.Intro.step_message"))
        elif step == "Properties":
            context["form_title"] = _("AssemblyDesignerWizard.Properties.form_title")
            # context["step_message"] = _("AssemblyDesignerWizard.Assembly.step_message")
        elif step == "InputParts":
            context["form_title"] = _("AssemblyDesignerWizard.Properties.form_title")
            context["add_new_item_to_formset_text"] = _("Add input part")
            context["extra_js_files"] = [
                "/js/netsted_formset.js",
            ]
        elif step == "Summary":
            context["form_title"] = _("AssemblyDesignerWizard.Summary.form_title")
            # context["step_message"] = _("AssemblyDesignerWizard.Summary.step_message")
            context["submit_text"] = _("AssemblyDesignerWizard.Summary.submit_text")
        # if context["step_message"] == _("NO STEP MESSAGE VALUE"):
        #     del context["step_message"]
        return context

    def get_form_initial(self, step):
        if step == "Properties" and self.is_edition is False:
            return dict(
                owner=self.request.user,
            )
        return super().get_form_initial(step)

    def get_form_kwargs(self, step=None):
        d = super().get_form_kwargs(step=step)
        if step == "Properties":
            d["owner"] = self.request.user
        # elif step == "Summary":
        #     d["agree_label"] = _("AssemblyDesignerWizard.Summary.agree_message")
        return d

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        if not hasattr(form, 'helper'):
            helper = FormHelper()
            helper.form_tag = False
            form.helper = helper
        return form

    def done(self, form_list, form_dict=None, **kwargs):
        with transaction.atomic():
            assembly = form_dict["Properties"].save()
            form_dict["InputParts"].instance = assembly
            for f in form_dict["InputParts"]:
                # f.is_valid()
                f.instance.assembly = assembly
                f.save()
        return render(
            self.request,
            'simple_message_page.html',
            context=dict(
                title=_("AssemblyDesignerWizard.done.title"),
                page_title=_("AssemblyDesignerWizard.done.title"),
                sub_message=mark_safe(_("AssemblyDesignerWizard.done.sub_message")),
            ),
        )
