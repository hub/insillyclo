import os
from zipfile import ZipFile
from Bio import SeqIO
from Bio.Seq import Seq


def get_genbank_file(name):
    zip_path = os.path.join(os.path.dirname(__file__), 'static/data/Plasmid_repository.zip')

    with ZipFile(zip_path, 'r') as zipped_gb:
        zipped_gb.printdir()
        return zipped_gb.extract(f"Plasmid_repository/{name}.gb")


def get_sequence(file):
    return SeqIO.read(file, 'genbank').seq


def get_nb_cut(seq, site_for, site_rev):
    return len(seq.split(site_for)) + len(seq.split(site_rev)) - 2


def reorder_seq(seq, site_for):
    splt_seq = seq.split(site_for)
    return splt_seq[1] + site_for + splt_seq[0]


def get_in_out_part(seq, site_rev):
    splt_seq = seq.split(site_rev)
    return (site_rev + splt_seq[1], splt_seq[0])


def get_sens_antisens(inpart, outpart, inter_s):
    out_sens = str(inpart[-(4 + inter_s) :] + outpart + inpart[: 4 + inter_s])
    in_sens = str(inpart[inter_s:-inter_s])
    out_antisens = str(Seq(out_sens).reverse_complement())
    in_antisens = str(Seq(in_sens).reverse_complement())
    return (out_sens, in_sens, out_antisens, in_antisens)


def get_fragments(part, enzyme=None, site_for='GGTCTC', site_rev='GAGACC', inter_s=1):
    gbfile = get_genbank_file(part)
    seq = get_sequence(gbfile)

    while (site_for not in seq) and (site_rev not in seq):
        shift = random.randint(30, 60)
        seq = seq[shift:] + seq[:shift]
        count += 1

        if count == 100:
            return None

        if get_nb_cut != 2:
            return None

    (out_part, in_part) = get_in_out_part(reorder_seq(seq, site_for), site_rev)

    return get_sens_antisens(in_part, out_part, inter_s)
