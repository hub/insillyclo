from django.shortcuts import render
from django.views.generic import ListView, DetailView

from assemblies import forms, models, utils


# Create your views here.


def index(request):
    context = dict(
        body_theme="with-logo",
    )
    return render(
        request=request,
        template_name='assemblies/index.html',
        context=context,
    )


def about(request):
    context = dict(
        body_theme="with-logo",
        fake_auth=True,
    )
    return render(
        request=request,
        template_name='assemblies/index.html',
        context=context,
    )


def show_fragment(request):
    context = dict(fragment=utils.get_fragments("pYTK002"))
    return render(request=request, template_name='assemblies/show_fragment.html', context=context)


def form_example(request):
    context = dict(
        title="ASSEMBLY DESIGNER (Fake form)", form=forms.AssemblyFormDemo(), form_title="ASSEMBLY Properties"
    )
    return render(
        request=request,
        template_name='assemblies/form_host.html',
        context=context,
    )


class AssemblyListView(
    ListView,
):
    model = models.Assembly

    def get_queryset(self):
        qs = super().get_queryset()
        # qs = self.model.annotate_is_empty(qs)
        # qs = qs.select_related('reviewer', 'candidate', 'stage')
        qs = qs.prefetch_related('input_parts', 'restriction_enzyme')
        return qs


class AssemblyDetailView(
    DetailView,
):
    model = models.Assembly

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.prefetch_related('input_parts', 'restriction_enzyme')
        return qs
