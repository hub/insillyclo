from django.contrib.auth import models as auth_models
from django.db import models
from django.db.models.functions import Upper
from django.utils.translation import gettext_lazy as _

from typing import List
import json


class UserManager(auth_models.UserManager):
    def create_user(self, email=None, password=None, **extra_fields):
        super().create_user(username=email, email=email, password=password, **extra_fields)

    def create_superuser(self, email=None, password=None, **extra_fields):
        super().create_superuser(username=email, email=email, password=password, **extra_fields)


class User(auth_models.AbstractUser):
    class Meta:
        ordering = [
            Upper("last_name"),
            Upper("first_name"),
        ]

    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    email = models.EmailField(unique=True)


class Separator(models.TextChoices):
    COMMA = ',', _('Comma')
    HYPHEN = '-', _('Hyphen')
    UNDERSCORE = '_', _('Underscore')
    DOT = '.', _('Dot')
    TILDE = '~', _('Tilde')


class RestrictionEnzyme(models.Model):
    class Meta:
        verbose_name = _('Restriction Enzyme')
        verbose_name_plural = _('Restriction Enzymes')

    name = models.CharField(
        max_length=255,
    )
    site_for = models.CharField(
        max_length=50,
        null=True,
    )
    site_rev = models.CharField(
        max_length=50,
        null=True,
    )
    inter_s = models.PositiveIntegerField(
        default=0,
    )

    def __str__(self):
        return self.name


class InputPart(models.Model):
    class Meta:
        verbose_name = _('Input Part')
        verbose_name_plural = _('Input Parts')
        unique_together = (('assembly', 'order'),)
        ordering = ('assembly', 'order')

    suffix_type_letters = 'abcdefghijkl'
    name = models.CharField(
        max_length=255,
    )
    assembly = models.ForeignKey(
        "assemblies.Assembly",
        on_delete=models.CASCADE,
        related_name='input_parts',
    )
    order = models.PositiveSmallIntegerField(
        default=1,
        null=False,
        blank=False,
    )
    is_in_output_plasmid_name = models.BooleanField()
    is_mandatory = models.BooleanField()
    number_of_subparts_str = models.CharField(
        null=True,
        blank=True,
        default=None,
        max_length=64,
    )
    subpart_separator = models.CharField(
        max_length=1,
        choices=Separator.choices,
        default=None,
        null=True,
    )
    __number_of_subparts = None
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
    )

    @property
    def is_separable(self) -> bool:
        return self.number_of_subparts_str is not None and self.number_of_subparts_str is not ""

    @property
    def number_of_subparts(self) -> List[int]:
        if self.number_of_subparts_str is None or self.number_of_subparts_str == '':
            return list()

        if self.__number_of_subparts is None:
            self.__number_of_subparts = json.loads(self.number_of_subparts_str)

        return self.__number_of_subparts

    @property
    def allowed_subparts(self) -> str:
        for cpt in self.number_of_subparts:
            if cpt == 1:
                yield str(self.order)
                continue
            yield [f'{str(self.order)}{l}' for l in self.suffix_type_letters[:cpt]]

    def __str__(self):
        return self.name


class Assembly(models.Model):
    class Meta:
        ordering = [
            Upper("name"),
        ]
        verbose_name = _('Assembly')
        verbose_name_plural = _('Assemblies')

    name = models.CharField(
        max_length=255,
    )
    is_public = models.BooleanField(
        default=False,
    )
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='assemblies',
    )
    output_separator = models.CharField(
        max_length=1,
        choices=Separator.choices,
        default=Separator.DOT,
    )
    is_output_typed = models.BooleanField(
        default=False,
    )
    restriction_enzyme = models.ForeignKey(
        RestrictionEnzyme,
        on_delete=models.PROTECT,
    )
    is_input_part_typed = models.BooleanField()
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
    )

    def get_is_output_typed_display(self):
        if self.is_output_typed:
            return _('is_output_typed display when true')
        return _('is_output_typed display when false')

    def __str__(self):
        return self.name
