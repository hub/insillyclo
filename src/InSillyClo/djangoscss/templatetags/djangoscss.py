import os
import random
import string
from pathlib import Path

from django import template
from django.conf import settings
from django.contrib.staticfiles import finders
from django.core.cache import cache
from django.db.models import QuerySet
from django.urls import reverse
from django.utils.html import escape
from django.utils.safestring import mark_safe

from djangoscss.storage import compile_scss

# credits : https://bitbucket.org/ad3w/django-sstatic

register = template.Library()

__USE_ABSOLUT_URL_FOR_STATIC = getattr(settings, "USE_ABSOLUT_URL_FOR_STATIC", False)


@register.simple_tag(takes_context=True)
def sstatic(context, path):
    url = cache.get(path)
    if url:
        return url
    if path.endswith(".scss"):
        path = get_compiled_css_from_scss(path)

    url = compute_url_for_path(path)
    if __USE_ABSOLUT_URL_FOR_STATIC:
        url = get_absolut_url(context["request"], url)
    cache.set(path, url, None)
    return url


def get_absolut_url(request, relative_url):
    return "%s://%s%s" % (getattr(settings, "DEFAULT_SCHEME", request.scheme), request.get_host(), relative_url)


def compute_url_for_path(path):
    '''
    Returns absolute URL to static file with versioning.
    '''
    try:
        full_path = os.path.join(settings.STATIC_ROOT, path[1:] if path[0] == '/' else path)
        # Get file modification time.
        mtime = os.path.getmtime(full_path)
        return '%s%s?%s' % (settings.STATIC_URL[:-1], path, mtime)
    except OSError:
        pass
    except TypeError:
        pass
    # Returns normal url if this file was not found in filesystem.
    return '%s%s?%s' % (
        settings.STATIC_URL[:-1],
        path,
        ''.join(random.choice(''.join((string.ascii_letters, string.digits))) for _ in range(4)),
    )


def get_compiled_css_from_scss(path):
    output_path = Path(path[:-5] + ".min.css")
    output_path = str(output_path.parent / ('_' + output_path.name))
    if not settings.SASS_COMPILATION_ON_THE_FLY:
        # computation disabled, assuming css is already built and served
        return output_path

    found_output_path = finders.find("." + output_path)
    if found_output_path is None:
        should_compile = True
    else:
        should_compile = os.path.getmtime(finders.find("." + path)) > os.path.getmtime(found_output_path)

    if should_compile:
        compile_scss(filename=finders.find("." + path, all=False))
    return output_path


@register.simple_tag(takes_context=True)
def active_if_is_current_path(context, url_name):
    return 'active' if context['request'].path == reverse(url_name) else ''


def __get_field_from_instance(field_name, obj):
    if type(obj) == QuerySet:
        f = obj.model._meta.get_field(field_name)
    else:
        f = obj._meta.get_field(field_name)
    return f


@register.filter
def field_verbose_name_and_help_text(obj, field_name, show_verbose_name=True):
    f = __get_field_from_instance(field_name, obj)
    verbose_name = escape(f.verbose_name.title())
    help_text = escape(f.help_text)
    if help_text == "":
        return verbose_name
    return mark_safe(
        f'{verbose_name if show_verbose_name else ""} <i class="bi bi-question-circle" title="{help_text}"></i>'
    )


@register.filter
def field_help_text(obj, field_name):
    return field_verbose_name_and_help_text(obj, field_name, False)
