import json
from typing import List

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.management import BaseCommand
from django.db import transaction
from faker import Faker

from assemblies import models


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--seed', default=0, type=int, nargs='?')
        parser.add_argument('--ignore-idempotent', action='store_true')

    @transaction.atomic
    def handle(self, *args, **options):
        ignore_idempotent = options['ignore_idempotent']
        seed = options['seed']
        load_demo(
            seed=seed,
            ignore_idempotent=ignore_idempotent,
        )


def load_demo(seed=0, ignore_idempotent=False):
    ignore_idempotent |= seed != 0
    fake = create_faker_instance(seed)

    public_owner = get_public_owner(fake)
    for i in range(3):
        create_assembly(
            fake=fake,
            is_public=True,
            owner=public_owner,
        )
    idempotent_check(fake, ignore_idempotent, 735)
    for i in range(2):
        create_assembly(
            fake=fake,
        )
    idempotent_check(fake, ignore_idempotent, 322)


def get_public_owner(fake):
    user, _ = get_user_model().objects.get_or_create(
        username='public.owner@pasteur.fr',
        defaults=dict(
            first_name='ada',
            last_name='lovelace',
            email='public.owner@pasteur.fr',
            password=make_password("test"),
        ),
    )
    return user


def create_faker_instance(seed):
    fake = Faker()
    fake.seed_instance(seed)
    return fake


def idempotent_check(fake, ignore_idempotent, expected_value):
    random_value = fake.random.randint(0, 1000)
    assert ignore_idempotent or expected_value == -1 or expected_value == random_value, (
        f'At this stage, we expected {expected_value} but got {random_value}. '
        'Either you broke idempotency (by using random package ?) so fixe it,'
        ' or changed the code and need to change the expected_value'
    )


def create_user(fake, test_pwd="") -> models.User:
    first_name, last_name = fake.first_name(), fake.last_name()
    email = f'{first_name}.{last_name}@pasteur.fr'.lower()
    user, created = models.User.objects.get_or_create(
        username=email,
        first_name=first_name,
        last_name=last_name,
        email=email,
        defaults=dict(password=test_pwd),
    )
    return user


def random_sub_array(fake: Faker, array: list, cpt: int = None, min_cpt: int = 0, max_cpt: int = None):
    if cpt is None:
        # there is a bug, it should normally be
        # cpt = fake.random.randint(min_cpt, len(array))
        # but test were written with len(array) - 1, so fixing it break multiple test, instead
        # patching when it crashed i.e when there is only one profile
        cpt = fake.random.randint(min_cpt, max(1, len(array) - 1))
    if max_cpt is None:
        max_cpt = len(array) - 1
    assert cpt >= 0
    assert cpt <= len(array)
    pos = set()
    while len(pos) < cpt:
        pos.add(fake.random.randint(0, max_cpt))
    for i in pos:
        yield array[i]


def create_input_part(
    *,
    fake: Faker,
    assembly: models.Assembly,
    order: int = None,
) -> models.InputPart:
    if fake.random_int(0, 1) == 1:
        number_of_subparts_str = json.dumps(list(random_sub_array(fake, list(range(1, 5)), min_cpt=1)))
    else:
        number_of_subparts_str = None
    if order is None:
        order = assembly.input_parts.count() + 1
    ip, _ = models.InputPart.objects.get_or_create(
        name=fake.word(),
        assembly=assembly,
        order=order,
        is_in_output_plasmid_name=fake.random_int(0, 1) == 1,
        is_mandatory=fake.random_int(0, 1) == 1,
        subpart_separator=models.Separator.choices[
            fake.random_int(
                0,
                len(models.Separator.choices) - 1,
            )
        ][0],
        number_of_subparts_str=number_of_subparts_str,
    )
    return ip


def create_assembly(
    *,
    fake: Faker,
    is_public: bool = False,
    owner: models.User = None,
) -> models.Assembly:
    if owner is None:
        owner = create_user(fake)
    o, _ = models.Assembly.objects.get_or_create(
        name=fake.sentence(fake.random_int(2, 6)),
        is_public=is_public,
        owner=owner,
        is_input_part_typed=fake.random_int(0, 1) == 1,
        output_separator=models.Separator.choices[
            fake.random_int(
                0,
                len(models.Separator.choices) - 1,
            )
        ][0],
        restriction_enzyme=models.RestrictionEnzyme.objects.all()[
            fake.random_int(
                0,
                models.RestrictionEnzyme.objects.count() - 1,
            )
        ],
    )
    for j in range(
        fake.random.randint(2, 8),
    ):
        create_input_part(fake=fake, assembly=o)
    return o
