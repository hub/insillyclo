from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms import layout
from django.forms import formset_factory
from django.utils.translation import gettext as _

from assemblies.models import Assembly, InputPart

SEP_CHOICES = (
    (".", "Dot"),
    ("-", "dash"),
    ("~", "tilde"),
)


class AssemblyForm(forms.ModelForm):
    class Meta:
        model = Assembly
        fields = (
            'owner',
            'name',
            'is_output_typed',
            'output_separator',
            'restriction_enzyme',
            'is_input_part_typed',
        )
        widgets = {
            'owner': forms.HiddenInput(),
        }

    def __init__(self, owner, *args, **kwargs):
        self.owner = owner
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data['owner'] = self.owner
        return cleaned_data


class InputPartForm(forms.ModelForm):
    class Meta:
        model = InputPart
        fields = (
            'name',
            'is_in_output_plasmid_name',
            'is_mandatory',
            'number_of_subparts_str',
            'subpart_separator',
            'order',
        )
        widgets = {
            # 'order': forms.HiddenInput(),
            # 'number_of_subparts_str': forms.HiddenInput(),
        }

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = layout.Layout(
        layout.Row(
            layout.Column(
                'order',
                'name',
                'is_in_output_plasmid_name',
                'is_mandatory',
                'number_of_subparts_str',
                'subpart_separator',
                layout.Div(
                    "DELETE",
                    css_class='d-none formset-delete-parent',
                ),
            )
        )
    )

    is_separable_in_sub_parts = forms.BooleanField(
        label=_("InputPartForm.is_separable_in_sub_parts.label"),
        help_text=_("InputPartForm.is_separable_in_sub_parts.help_text"),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        # self.assembly = assembly
        super().__init__(*args, **kwargs)

    # def clean(self):
    #     cleaned_data = super().clean()
    #     cleaned_data['assembly'] = self.assembly
    #     return cleaned_data


InputPartFormFormSet = formset_factory(
    InputPartForm,
    can_delete=True,
    extra=0,
    min_num=1,
    validate_min=True,
)


class AssemblyFormDemo(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = layout.Layout(
            layout.Fieldset(
                _('Name'),
                'name',
            ),
            layout.Fieldset(
                _('Naming convention for the Output Separator'),
                'separator',
                'separatorbis',
            ),
            layout.Fieldset(
                _('Type of the output plasmid'),
                'typed',
            ),
        )

    name = forms.CharField(
        label='',
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Name',
            }
        ),
    )
    separator = forms.ChoiceField(
        label='When computing the name of the output plasmid, what separator would you '
        'like to use to separate your input part names ?',
        help_text=" example: with ' - ' as separator, assembling pTDH3, GFP, tADH1 and a "
        "backbone would give pTDH3-GFP-tADH1-backbone",
        choices=SEP_CHOICES,
        widget=forms.Select(),
    )
    separatorbis = forms.ChoiceField(
        label='anotherone in this section',
        help_text=" example: with ' - ' as separator, assembling pTDH3, GFP, tADH1 and a "
        "backbone would give pTDH3-GFP-tADH1-backbone",
        choices=SEP_CHOICES,
        widget=forms.Select(),
    )
    typed = forms.BooleanField(
        label='Define part type for the output plasmid',
        help_text="To support Modular Cloning, InSillyClo allows you to assign types to genetic "
        "parts. Most Modular Cloning kit include iterative assemblies where the output "
        "plasmids of the first assembly are the input of the second. In such case it is "
        "important to be able to assign a type to the output plasmids of the first "
        "assembly.",
        widget=forms.CheckboxInput(),
    )


class AgreeForm(forms.Form):
    agree = forms.BooleanField(
        initial=False,
        required=True,
        label="",
    )

    def __init__(self, agree_label=None, agree_help_text=None, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        super().__init__(*args, **kwargs)
        if agree_label is None:
            del self.fields["agree"]
        else:
            self.fields["agree"].label = agree_label
            self.fields["agree"].help_text = agree_help_text
