#!/usr/bin/env bash

touch tokens.sh
source ./tokens.sh # put in this file any needed secret env var such as `export SECRET_KEY="..."`

NAMESPACE="insillyclo-dev"
#VALUES_OVERRIDE_FILENAME="values.prod.yaml" # uncomment for prod
CI_PROJECT_NAMESPACE="hub"
CI_PROJECT_NAME="insillyclo"
CI_REGISTRY="registry-gitlab.pasteur.fr"
CI_REGISTRY_IMAGE="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/"
CI_COMMIT_SHA=$(git log --format="%H" -n 1)
# CI_COMMIT_SHA="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
CI_COMMIT_REF_SLUG=$(git branch --show | cut -c1-63 | sed 's/-*$//g')
REL_NAME=$CI_COMMIT_REF_SLUG
REL_NAME=$([[ $REL_NAME == [0-9]* ]] && echo "b${REL_NAME}" || echo $REL_NAME)
REL_NAME=$(echo $REL_NAME | cut -c1-53)
CHART_LOCATION="chart"
DEBUG="True"
PUBLICLY_OPEN="False"
IS_PROD="False"

export ACTION="upgrade --install"
export ACTION="template --debug"

helm ${ACTION} --namespace=${NAMESPACE} \
    --set commitSha=${CI_COMMIT_SHA} \
    --set registry=${CI_REGISTRY_IMAGE}${CI_COMMIT_REF_SLUG} \
      --values ./chart/values.yaml \
      --values ./chart/${VALUES_OVERRIDE_FILENAME:-values.yaml} \
    ${REL_NAME} ./${CHART_LOCATION}/
