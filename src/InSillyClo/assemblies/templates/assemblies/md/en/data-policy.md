<blockquote>
<b>This page is under construction.</b>
</blockquote>

We are committed to safeguarding your privacy and ensuring the confidentiality of your data. Rest assured, your data will not be accessed by any third parties, nor will it be stored on our servers. Every interaction you have with our application remains secure and private.

To enhance your control over your data, we offer the option to run our application locally on your computer. By downloading the Docker Desktop application, you can operate our software in local mode, ensuring that all data processing occurs on your device. This approach provides an additional layer of privacy, as no data will be transmitted to or processed by external servers.

We believe in empowering our users with full transparency and control over their information. For step-by-step guidance on how to set up and use our application in local mode, please refer to our [tutorial](/tutorial)

Your trust is important to us, and we are dedicated to maintaining it through stringent data protection practices.