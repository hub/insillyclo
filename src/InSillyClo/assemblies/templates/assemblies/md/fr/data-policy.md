<blockquote>
<b>Cette page est en cours de construction.</b>
</blockquote>

Nous nous engageons à protéger votre vie privée et à garantir la confidentialité de vos données. Soyez assuré que vos données ne seront pas accessibles à des tiers, ni stockées sur nos serveurs. Chaque interaction que vous avez avec notre application reste sécurisée et privée.

Pour renforcer votre contrôle sur vos données, nous offrons la possibilité d'exécuter notre application localement sur votre ordinateur. En téléchargeant l'application Docker Desktop, vous pouvez utiliser notre logiciel en mode local, garantissant que tout le traitement des données se fait sur votre appareil. Cette approche offre une couche supplémentaire de confidentialité, car aucune donnée ne sera transmise ou traitée par des serveurs externes.

Nous croyons en l'autonomisation de nos utilisateurs grâce à une transparence totale et un contrôle complet sur leurs informations. Pour un guide détaillé sur la configuration et l'utilisation de notre application en mode local, veuillez consulter notre [tutoriel](/tutorial).

Votre confiance est importante pour nous, et nous nous engageons à la maintenir grâce à des pratiques rigoureuses de protection des données.
